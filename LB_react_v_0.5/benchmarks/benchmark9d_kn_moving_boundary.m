%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%reactive wall test 
%Note: this example is carried out in Lattice boltzmann units
%in this example D =1/6 (as tau by default is 1)
%% Matlab Intialization
clear all
clc
%matlabpool open 
initial_time = cputime;
%add path for LB toolbox
addpath '..\..\LB_React_v_0.5\LB_functions\'

%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =100;           %length of domain in ltu
delx                   =0.5;             %lattice spacing in ltu
Clhs                   =1;          %concentration at LHS boundary
ts                     =80000 ;      %final time in sec
Ceq                    =9.5;           %ceq
Cs                     =10;          %conc in solid
R                      =1e-4;          %reaction rate
S0                     =60;            %inital location of interface         
%________________________________________________________________________
%derived input:

%number of nodes
ly=(domain_length/delx)+2;

%x co-ordinates for node
x=0:delx:domain_length+delx;
x=x-delx/2;

%Assign solid nodes and conc in solid node matrix
sln=x>S0;
Slnmol = Cs* sln;

%% intialization

%initialization of f's
ind = find(sln, 1, 'first');
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;
f(1:ind-2,1)=Clhs*1/6;
f(1:ind-2,3)=Clhs*1/6;
f(1:ind-2,2)=Clhs*4/6;
f(ind-1,1)=Ceq*1/6;
f(ind-1,3)=Ceq*1/6;
f(ind-1,2)=Ceq*4/6;

%intialization of other variables
%sink mat
sink(1:ly)= 0;

%time steps to get output
OutTime=0:500:ts;

%shift distance
shift(1:length(OutTime))=0;
shift(1)=0;
count=2;

%% LB calculations
for t =1:ts

%% LB-kernel------------------------------------------

%% call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f,'sln',sln);

%% call equillibrium distribution function 
feq = equillibrium_distribution_function(conc,'sln',sln);

%% compute sink term
sink(1:ly)= 0;
for i = 1:ly-1
    if ~sln(i)
        if sln(i+1)
           sink(i) = R*(Ceq-Clhs);
        end
    end
end

%% call collision function
f = collision(f,feq,'sln',sln,'source-sink',sink,'conc',conc);

%% call propogation function
f = propogation(f,'sln',sln);

%% call boundary condition implementation
% f =  boundary_condition(f,'top_conc_bc','c_0',Clhs ,'sln',sln,'type','midway');

%% modify solid node
for k=2:ly
    if sln(k)
        Slnmol(k)= Slnmol(k) - sink(k-1);        
    end
        
    if (Slnmol(k)/Cs)<=0.5
         sln(k)=0;
        if k<ly
            Slnmol(k+1)=Slnmol(k+1)+Slnmol(k);
            Slnmol(k)=0;
        end
        
    end
    
end
%% LB-kernel ends ---------------------------------------------------------
%% output for plot
if ismember(t,OutTime)
    display(['Time step finished:    ',num2str(t)])
    ind = find(sln, 1, 'first');
    shift(count) = (x(ind)-(delx/2))-S0;
    count = count+1;  
    
end

end
%% Analytical solution
C =-(Clhs-Ceq)/(Cs-Clhs);
% calcuate shift of interface
shiftAny =C*R*OutTime*delx;

%% plot output
plot(OutTime,shiftAny,'r',OutTime,shift,'--k')
xlabel('Time (Sec)')
ylabel('Shift (m)')
legend('Analytical','LB','location','Southeast')

%% display simulation time
final_time = cputime;
total_time = final_time -initial_time;
display(['Time taken for simulation is ', num2str(total_time),' sec'])
%matlabpool close
