%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%testing of bounce back condition

%% Matlab Intialization
clear all
initial_time = cputime;
%add path for LB toolbox
addpath '..\..\LB_React\LB_functions\'

%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =3;               %length of domain in m
delx                   =.1;              %lattice spacing in m 
diffusivity_physical   =0.04;            %Diffusivity of solute/ion in m^2/s
conc_lhs_pyhsical      =100;             %concentration at LHS boundary
ts_phy                 =500;               %time in sec                 
%________________________________________________________________________

%% calling unit converter
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusivity_physical);

%% convert input parameters in form of lb
conc_lhs_lb = conc_lhs_pyhsical/c0;
ts = round(ts_phy/t0);

%% intialization
ly=(domain_length/delx)+2;
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;
f(1,1)=conc_lhs_lb*1/6;f(1,2)=conc_lhs_lb*4/6;f(1,3)=conc_lhs_lb*1/6;

%solid node at end
sln(1:ly)=0;
sln(ly)=1;

%% LB calculations
for t =1:ts
   display(['Time:    ',num2str(t*t0)])
%call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f,'sln',sln);

%call equillibrium distribution function 
feq = equillibrium_distribution_function(conc,'sln',sln);

%call collision function
f = collision(f,feq,'sln',sln);

%call propogation function
f = propogation(f,'sln',sln);

%call boundary condition implementation
f =  boundary_condition(f,'top_conc_bc','c_0',conc_lhs_lb,'sln',sln,'type','midway','c_1',conc(2));

end

%% plot output
x=0:delx:domain_length+delx;
x=x-delx/2;
conc_phy = conc*c0;
%analytical solution
conc_any = (conc_lhs_pyhsical)*erfc(x/sqrt(4*diffusivity_physical*ts_phy));
figure(1)
plot(x(:),conc_phy(:),'k','linewidth',1.5)
hold on
plot(x(:),conc_any(:),'--r',x(:),conc_any(:),'o','MarkerfaceColor','r','MarkeredgeColor','r')
hold off
legend('on')
legend('LB','analytical')
xlabel('distance(m)')
ylabel('conc(mol/m3)')


%% display simulation time
final_time = cputime;
total_time = final_time -initial_time;
display(['Time taken for simulation is ', num2str(total_time),' sec'])

