%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%flux bc testing

%% Matlab Intialization
clear all
tic;
%add path for LB toolbox
addpath '..\..\LB_React\LB_functions\'
%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =3;               %length of domain in m
delx                   =.1;              %lattice spacing in m 
diffusivity_physical   =0.01;            %Diffusivity of solute/ion in m^2/s
conc_lhs_pyhsical      =100;             %concentration at LHS boundary
ts_phy                 =5;               %time in sec                 
%________________________________________________________________________

%% calling unit converter
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusivity_physical);

%% convert input parameters in form of lb
conc_lhs_lb = conc_lhs_pyhsical/c0;
ts = round(ts_phy/t0);

%% intialization
ly=(domain_length/delx)+1;
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;
f(1,1)=conc_lhs_lb*1/6;f(1,2)=conc_lhs_lb*4/6;f(1,3)=conc_lhs_lb*1/6;

%% LB calculations
for t =1:ts
   display(['Time:    ',num2str(t*t0)])
%call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f);

%call equillibrium distribution function 
feq = equillibrium_distribution_function(conc);

%call collision function
f = collision(f,feq);

%call propogation function
f = propogation(f);

%call boundary condition implementation
f =  boundary_condition(f,'top_conc_bc','c_0',conc_lhs_lb,'bottom_flux_bc','flux_ly',0);

end

%% plot output
x=0:delx:domain_length;
conc_phy = conc*c0;
%analytical solution
conc_any = (conc_lhs_pyhsical)*erfc(x/sqrt(4*diffusivity_physical*ts_phy));
figure(1)
plot(x(:),conc_phy(:),'k','linewidth',1.5)
hold on
plot(x(:),conc_any(:),'--r',x(:),conc_any(:),'o','MarkerfaceColor','r','MarkeredgeColor','r')
hold off
legend('on')
legend('LB','analytical')
xlabel('distance(m)')
ylabel('conc(mol/m3)')


%% display simulation time
display(['Time taken for simulation is ', num2str(toc),' sec'])

