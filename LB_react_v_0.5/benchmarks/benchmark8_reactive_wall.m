%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%reactive wall test

%% Matlab Intialization
clear all
initial_time = cputime;
%add path for LB toolbox
addpath '..\..\LB_React_v_0.5\LB_functions\'

%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =3;               %length of domain in m
delx                   =.05;              %lattice spacing in m 
diffusivity_physical   =0.01;            %Diffusivity of solute/ion in m^2/s
conc_lhs_pyhsical      =500;             %concentration at LHS boundary
ts_phy                 =2500;            %time in sec       
k                      =2;             %reaction rate in m/s 
ceq                    =0;               %ceq
%________________________________________________________________________

%% calling unit converter
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusivity_physical);

%% convert input parameters in form of lb
conc_lhs_lb = conc_lhs_pyhsical/c0;
ts = round(ts_phy/t0);
k_lb = k* t0/l0;
ceq_lb =ceq/c0;
%% intialization
ly=(domain_length/delx)+2;
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;
f(1,1)=conc_lhs_lb*1/6;f(1,2)=conc_lhs_lb*4/6;f(1,3)=conc_lhs_lb*1/6;

%solid node at end
sln(1:ly)=0;
sln(ly)=1;

%sink mat
sink(1:ly)=0;

%% LB calculations
for t =1:ts
   display(['Time:    ',num2str(t*t0)])
%call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f,'sln',sln);

%call equillibrium distribution function 
feq = equillibrium_distribution_function(conc,'sln',sln);

%sink term
sink(ly-1) = -(conc(ly-1)-ceq_lb)*k_lb;

%call collision function
f = collision(f,feq,'sln',sln,'source-sink',sink,'conc',conc);

%call propogation function
f = propogation(f,'sln',sln);

%call boundary condition implementation
f =  boundary_condition(f,'top_conc_bc','c_0',conc_lhs_lb,'sln',sln,'type','midway');

end

%% plot output
x=0:delx:domain_length+delx;
x=x-delx/2;
conc_phy = conc*c0;

%compute analytical solution
Da =(k*domain_length/diffusivity_physical);
disp(['Da= ',num2str(Da)])
conc_any = conc_lhs_pyhsical*(1-(Da/(1+Da))*(x/domain_length));

figure(1)
plot(x(2:length(x)-1),conc_phy(2:length(conc_phy)-1),'r','linewidth',1.0)
hold on
plot(x(2:length(x)-1),conc_any(2:length(conc_any)-1),'--k','linewidth',1.0)
hold off
Er =abs( sum((conc_phy(2:length(x)-1)-conc_any(2:length(x)-1)))./sum(conc_any(2:length(x)-1)))*100;
disp(['ErrorNorm(in %)= ',num2str(Er)])
xlabel('distance(m)')
ylabel('conc(mol/m3)')


%% display simulation time
final_time = cputime;
total_time = final_time -initial_time;
display(['Time taken for simulation is ', num2str(total_time),' sec'])

