%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%instantenous source release

%% Matlab Intialization
clear all
initial_time = cputime;
%add path for LB toolbox
addpath '..\..\LB_React\LB_functions\'

%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =3;               %length of domain in m
delx                   =.03;             %lattice spacing in m 
diffusivity_physical   =0.01;            %Diffusivity of solute/ion in m^2/s
conc_center            =100;             %concentration at LHS boundary mol/m^3/m
ts_phy                 =3;               %time in sec                 
%________________________________________________________________________

%% calling unit converter
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusivity_physical);

%% convert input parameters in form of lb
conc_center_lb = conc_center/(l0*c0);
ts = round(ts_phy/t0);

%% intialization
ly=(domain_length/delx)+1;
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;
f(round(ly/2),1)=conc_center_lb *1/6;f(round(ly/2),2)=conc_center_lb *4/6;f(round(ly/2),3)=conc_center_lb *1/6;

%% LB calculations
for t =1:ts
   display(['Time:    ',num2str(t*t0)])
%call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f);

%call equillibrium distribution function 
feq = equillibrium_distribution_function(conc);

%call collision function
f = collision(f,feq);

%call propogation function
f = propogation(f);

%call boundary condition implementation
f =  boundary_condition(f,'top_periodic_bc','bottom_periodic_bc');

end

%% plot output
x=0:delx:domain_length;
conc_phy = conc*c0;
%analytical solution
conc_any = (conc_center/sqrt(4*pi*diffusivity_physical*ts_phy))*exp(-(x-(domain_length/2)).^2/(4*diffusivity_physical*ts_phy));
figure(1)
plot(x(:),conc_phy(:),'k','linewidth',1.5)
hold on
plot(x(:),conc_any(:),'--r',x(:),conc_any(:),'o','MarkerfaceColor','r','MarkeredgeColor','r')
hold off
legend('on')
legend('LB','analytical')
xlabel('distance(m)')
ylabel('conc(mol/m3)')


%% display simulation time
final_time = cputime;
total_time = final_time -initial_time;
display(['Time taken for simulation is ', num2str(total_time),' sec'])

