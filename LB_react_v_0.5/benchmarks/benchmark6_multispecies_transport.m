%Multicomponent diffusion using LB tool box
%% Matlab Intialization
clear all
initial_time = cputime;
%add path for LB toolbox
addpath '..\..\LB_React\LB_functions\'
%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =3;               %length of domain in m
delx                   =.1;              %lattice spacing in m 
n_species              =7;               %number of component species 
diffusivity_physical   =.1;              %Diffusivity of solute/ion in m^2/s
ts_phy                 =4;               %time in sec 
species(1).intial_conc = 0;              %species 1 domain intial condition
species(1).conc_lhs = 100;               %species 1 domain lhs bc
species(1).conc_rhs = 0;                 %species 1 domain rhs bc
species(2).intial_conc = 0;              %species 2 domain intial condition
species(2).conc_lhs = 150;               %species 2 domain lhs bc
species(2).conc_rhs = 0;                 %species 2 domain rhs bc
species(3).intial_conc = 0;              %species 3 domain intial condition
species(3).conc_lhs = 50;                %species 3 domain lhs bc
species(3).conc_rhs = 0;                 %species 3 domain rhs bc
species(4).intial_conc = 0;              %species 4 domain intial condition
species(4).conc_lhs = 200;               %species 4 domain lhs bc
species(4).conc_rhs = 0;                 %species 4 domain rhs bc
species(5).intial_conc = 0;              %species 5 domain intial condition
species(5).conc_lhs = 300;               %species 5 domain lhs bc
species(5).conc_rhs = 0;                 %species 5 domain rhs bc
species(6).intial_conc = 0;              %species 6 domain intial condition
species(6).conc_lhs = 120;               %species 6 domain lhs bc
species(6).conc_rhs = 0;                 %species 6 domain rhs bc
species(7).intial_conc = 0;              %species 7 domain intial condition
species(7).conc_lhs = 250;               %species 7 domain lhs bc
species(7).conc_rhs = 0;                 %species 7 domain rhs bc
%________________________________________________________________________


%% unit conversion from physical system to lb system
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusivity_physical);

for i =1:n_species
    species(i).intial_conc_lb =species(i).intial_conc/c0;
    species(i).conc_lhs_lb = species(i).conc_lhs/c0;
    species(i).conc_rhs_lb = species(i).conc_rhs/c0;
end

%% setting up problem
%number of nodes
ly = (domain_length/delx)+1;

%number of timestep iterations
ts = round(ts_phy/t0);

%% inital condition
%intialization of f's
w(1)=1/6;w(2)=4/6;
for i=1:n_species;
    species(i).f(1:ly,1)= species(i).intial_conc_lb *w(1);
    species(i).f(1:ly,3)= species(i).intial_conc_lb *w(1);
    species(i).f(1:ly,2)= species(i).intial_conc_lb *w(2);
    
    %at x=0 boundary
    species(i).f(1,1)   = species(i).conc_lhs_lb *w(1);
    species(i).f(1,3)   = species(i).conc_lhs_lb *w(1);
    species(i).f(1,2)   = species(i).conc_lhs_lb *w(2);
    
    %at x=ly boundary
    species(i).f(ly,1)   = species(i).conc_rhs_lb *w(1);
    species(i).f(ly,3)   = species(i).conc_rhs_lb *w(1);
    species(i).f(ly,2)   = species(i).conc_rhs_lb *w(2);
end


%% LB kernel for multiple species with constant conc boundary conditions 

for t =1:ts
    display(['Time:    ',num2str(t*t0)])
    
     for i=1:n_species;
         
         %call macroscopic quantites calculation function
         [species(i).conc,species(i).flux] = conc_flux_computation(species(i).f);
         output.conc(:,i) =species(i).conc' * c0;
         output.flux(:,i) =species(i).flux' * f0;
         
         %call equillibrium distribution function
         species(i).feq = equillibrium_distribution_function(species(i).conc);
         
         %call collision function
         species(i).f = collision(species(i).f,species(i).feq);
         
         %call propogation function
         species(i).f = propogation(species(i).f);
         
         %call boundary condition implementation
         species(i).f =  boundary_condition(species(i).f,'top_conc_bc','c_0',species(i).conc_lhs_lb,'bottom_conc_bc','c_ly',species(i).conc_rhs_lb);
         
     end
     
     
end
%% plot output
x = 0:delx:domain_length;
for i=1:n_species
    species(i).conc_any = (species(i).conc_lhs)*erfc(x/sqrt(4*diffusivity_physical*ts_phy));
end
figure(1)
plot(x(:),output.conc(:,1) ,x(:),output.conc(:,2) ,...
     x(:),output.conc(:,3) ,x(:),output.conc(:,4) ,...
     x(:),output.conc(:,5) ,x(:),output.conc(:,6) ,...
     x(:),output.conc(:,7),'linewidth',1.5)
hold on
plot(x(:),species(1).conc_any,'--',x(:),species(2).conc_any,'--',...
     x(:),species(3).conc_any,'--',x(:),species(4).conc_any,'--',...
     x(:),species(5).conc_any,'--',x(:),species(6).conc_any,'--',...
     x(:),species(7).conc_any,'--')
legend('on')
legend('LB species 1','LB species 2','LB species 3','LB species 4','LB species 5',...
       'LB species 6','LB species 7','analytical species 1','analytical species 2',...
       'analytical species 3','analytical species 4','analytical species 5',...
       'analytical species 6','analytical species 7');
xlabel('distance(m)')
ylabel('conc(mol/m3)')


%% display simulation time
final_time = cputime;
total_time = final_time -initial_time;
display(['Time taken for simulation is ', num2str(total_time),' sec'])

%% end of the code