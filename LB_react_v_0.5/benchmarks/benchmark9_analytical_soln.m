%% Input:
ceq  = 0.3;
clhs = 0.1;
D    =0.01;
Time =0:100:10000;

%% solve transdental equation to solve l
C =2*(ceq - clhs)/(1-ceq);
Tranfunc =@(l)(pi^0.5*l*exp(l^2)*erfc(l))+0.5*C;
l=fzero(Tranfunc,1);
%% calcuate shift of interface
Shift=-2*l*sqrt(D*Time);
%% plot analytical solution
Time = Time';
Shift=Shift';
plot(Time,Shift,'-o')

