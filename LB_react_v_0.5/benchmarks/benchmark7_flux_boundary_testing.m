%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%flux bc testing

%% Matlab Intialization
clear all
tic;
%add path for LB toolbox
addpath '..\..\LB_React\LB_functions\'
%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =.08;                                %length of domain in m
delx                   =0.08/100;                            %lattice spacing in m 
u_physical             =0.00027777/100;                     %vel m/s
diffusivity_physical   =0.2/100*u_physical;                 %Diffusivity of solute/ion in m^2/s
flux_lhs_pyhsical      =0.0012*u_physical;                  %concentration at LHS boundary
ts_phy                 =86400;                              %time in sec                 
%________________________________________________________________________

%% calling unit converter
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusivity_physical);

%% convert input parameters in form of lb
flux_lhs_lb = flux_lhs_pyhsical/f0;
u = u_physical/v0;
ts = round(ts_phy/t0);

%% intialization
ly=(domain_length/delx)+1;
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;

%% LB calculations
for t =1:ts
   display(['Time:    ',num2str(t*t0)])
%call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f,'u',u);

%call equillibrium distribution function 
feq = equillibrium_distribution_function(conc,'u',u);

%call collision function
f = collision(f,feq,'u',u);

%call propogation function
f = propogation(f,'u',u);

%call boundary condition implementation
f =  boundary_condition(f,'top_flux_bc','flux_0',flux_lhs_lb,'u',u);

%result to save
result(t)=conc(ly)*c0;

end

 %% plot output
t=0:t0:ts_phy;
conc_phy =result;
figure(1)
plot(t(:),conc_phy(:),'k','linewidth',1.5)
xlabel('time(sec)')
ylabel('conc(mol/m3)')


%% display simulation time
display(['Time taken for simulation is ', num2str(toc),' sec'])

