%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%reactive wall test

%% Matlab Intialization
clear all
initial_time = cputime;
%add path for LB toolbox
addpath ('..\..\LB_React_v_0.5\LB_functions\')

%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =10;            %length of domain in m
delx                   =.1;            %lattice spacing in m 
diffusivity_physical   =0.01;        %Diffusivity of solute/ion in m^2/s
conc_lhs_pyhsical      =0.01;           %concentration at LHS boundary
ts_phy                 =3500;         %time in sec       
k                      =1;             %reaction rate in m/s 
ceq                    =0.05;           %ceq
%________________________________________________________________________

%derived input:

%number of nodes
ly=(domain_length/delx)+2;

%solid nmol mat (conc of solid phases)
SlnNmol(2:ly)=1; %mol/m^3

%solid node mat
sln(1:ly)=0;
sln(2:ly)=1;

%% calling unit converter
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusivity_physical);

%% convert input parameters in form of lb
conc_lhs_lb = conc_lhs_pyhsical/c0;
ts = round(ts_phy/t0);
k_lb = k* t0/l0;
ceq_lb =ceq/c0;
SlnNmol_LB = SlnNmol/c0;
%% intialization
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;
f(2,1)=ceq_lb*1/6;
f(2,2)=ceq_lb*4/6;
f(2,3)=ceq_lb*1/6;

%intialization of other variables
%sink mat
sink(1:ly)= 0;

old_nsolid = sum(sln);

solidNo(1) = old_nsolid;

Time(1)= 0;

count=2;
%% LB calculations
for t =1:ts

display(['Time:    ',num2str(t*t0)])
   
%call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f,'sln',sln);

%call equillibrium distribution function 
feq = equillibrium_distribution_function(conc,'sln',sln);

%compute sink term
for i = 1:ly-1
    if ~sln(i)
        if sln(i+1)
            sink(i) = -(conc(i)-ceq_lb)*k_lb;
        end
    end
end

%call collision function
f = collision(f,feq,'sln',sln,'source-sink',sink,'conc',conc);

%call propogation function
f = propogation(f,'sln',sln);

%call boundary condition implementation
f =  boundary_condition(f,'top_conc_bc','c_0',conc_lhs_lb,'sln',sln,'type','midway');

%modify solid node
for i=2:ly
    if sln(i)
       SlnNmol_LB(i)= SlnNmol_LB(i) - sink(i-1);
    end
    
%     if i~=ly
%         if SlnNmol_LB(i)<0
%             SlnNmol_LB(i+1)=SlnNmol_LB(i+1)+SlnNmol_LB(i);
%             SlnNmol_LB(i)=0;
%         end
%     end
    
    if SlnNmol_LB(i)<=0
        sln(i)=0;
    end
    
end

%output for plot
new_nsolid = sum(sln);
% if old_nsolid ~= new_nsolid
solidNo(t)= new_nsolid;
Time(t) = t*t0;
count=count+1;
% end
% old_nsolid = new_nsolid;
end

%% plot output
Inter_disp = -(solidNo -solidNo(1)).*delx;
Inter_disp =Inter_disp';
Time =Time';
plot(Time,Inter_disp,'-o')


%% display simulation time
final_time = cputime;
total_time = final_time -initial_time;
display(['Time taken for simulation is ', num2str(total_time),' sec'])

