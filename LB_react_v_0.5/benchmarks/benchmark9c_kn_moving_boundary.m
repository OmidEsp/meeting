%1-D diffusion problem using LB D1Q3 diffusion toolbox 
%reactive wall test 
%Note: this example is carried out in Lattice boltzmann units
%in this example D =1/6 (as tau by default is 1)
%% Matlab Intialization
clear all
clc
initial_time = cputime;
%add path for LB toolbox
addpath '..\..\LB_React_v_0.5\LB_functions\'

%% Input:
%________________________________________________________________________
%Physical properties:(To be specified by users)
domain_length          =5e-2;         %length of domain in m
delx                   =1e-3;          %lattice spacing in m
ClhsPhy                =0.3;           %concentration at LHS boundary
tphy                   =300;          %final time in sec
Dphy                   =0.001;           %diffusivity in m^2/s
CeqPhy                 =0.5;           %ceq
CsPhy                  =1;             %amt of solid
kphy                   =1e-4;          %reaction rate
S0                     =4e-2;          %inital location of interface         
%________________________________________________________________________
%derived input:

%number of nodes
ly=(domain_length/delx)+2;

%x co-ordinates for node
x=0:delx:domain_length+delx;
x=x-delx/2;

%Assign solid nodes and conc in solid node matrix
sln=x>S0;
SlnmolPhy = CsPhy* sln;

%% unit conversion
[l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,Dphy);

%converting physical variables into lb units
Clhs   =ClhsPhy/c0;
Ceq    =CeqPhy/c0;
Slnmol = SlnmolPhy/c0;
r0      =kphy *t0/l0;
ts = round(tphy/t0);


%% intialization

%initialization of f's
ind = find(sln, 1, 'first');
f(1:ly,1)=0;f(1:ly,2)=0;f(1:ly,3)=0;
f(1:ind-2,1)=Clhs*1/6;
f(1:ind-2,3)=Clhs*1/6;
f(1:ind-2,2)=Clhs*4/6;
f(ind-1,1)=Ceq*1/6;
f(ind-1,3)=Ceq*1/6;
f(ind-1,2)=Ceq*4/6;

%intialization of other variables
%sink mat
sink(1:ly)= 0;

%time steps to get output
OutTime=0:5000:ts;
%shift distance
shift(1:length(OutTime))=0;
shift(1)=0;
count=2;

%% LB calculations
for t =1:ts

%% LB-kernel------------------------------------------

%% call macroscopic quantites calculation function
[conc,flux] = conc_flux_computation(f,'sln',sln);

%% call equillibrium distribution function 
feq = equillibrium_distribution_function(conc,'sln',sln);

%% compute sink term
sink(1:ly)= 0;
for i = 1:ly-1
    if ~sln(i)
        if sln(i+1)
           sink(i) = r0*(Ceq-Clhs);
        end
    end
end

%% call collision function
f = collision(f,feq,'sln',sln,'source-sink',sink,'conc',conc);

%% call propogation function
f = propogation(f,'sln',sln);

%% call boundary condition implementation
f =  boundary_condition(f,'top_conc_bc','c_0',Clhs ,'sln',sln,'type','midway');

%% modify solid node
for k=2:ly
    if sln(k)
        Slnmol(k)= Slnmol(k) - sink(k-1);        
    end
        
    if (Slnmol(k)*c0/CsPhy)<=0.5
         sln(k)=0;
        if k<ly
            Slnmol(k+1)=Slnmol(k+1)+Slnmol(k);
            Slnmol(k)=0;
        end
    end
    
end
%% LB-kernel ends ---------------------------------------------------------
%% output for plot
if ismember(t,OutTime)
    display(['Time step finished:    ',num2str(t*t0)])
    ind = find(sln, 1, 'first');
    shift(count) = (x(ind)-(delx/2))-S0;
    count = count+1;  
    
end

end
%% Analytical solution
C =(CeqPhy - ClhsPhy)/(CsPhy-ClhsPhy);
% calcuate shift of interface
shiftAny =C*kphy*OutTime*t0;


%% plot output
plot(OutTime*t0,shiftAny,'r',OutTime*t0,shift,'--k')
xlabel('Time (Sec)')
ylabel('Shift (m)')
legend('Analytical','LB','location','Southeast')

%% display simulation time
final_time = cputime;
total_time = final_time -initial_time;
display(['Time taken for simulation is ', num2str(total_time),' sec'])

