function ftemp = propogation(f,varargin)
%propogation step with bounce back 
%(3)<-----(2)----->(1)
%input
ly=size(f);
ly=ly(1);
sln(1:ly)=0;
sln_r(1:ly)=0;

if ~isempty(varargin)
    for i=1:length(varargin)
        if strcmp(varargin{i},'sln')
            sln=varargin{i+1};
        end
        if strcmp(varargin{i},'sln_r')
            sln_r=varargin{i+1};
        end
    end
end

%intialization
ftemp =f;

%propogation at internal nodes with bounce back

%@ internal nodes
for i=2:(ly-1)
    if ~sln(i) && ~sln_r(i)
        if ((sln(i+1)==0 && sln_r(i+1)==0) && (sln(i-1)~=0||sln_r(i-1)~=0))
            ftemp(i+1,1)  =f(i,1);
            ftemp(i,1)    =f(i,3);
            ftemp(i,2)    =f(i,2);
        elseif ((sln(i-1)==0 && sln_r(i-1)==0) && (sln(i+1)~=0||sln_r(i+1)~=0))
            ftemp(i,3)    =f(i,1);
            ftemp(i-1,3)  =f(i,3);
            ftemp(i,2)    =f(i,2);
        elseif ((sln(i-1)~=0||sln_r(i-1)~=0) && (sln(i+1)~=0||sln_r(i+1)~=0))
            ftemp(i,3)    =f(i,1);
            ftemp(i,1)    =f(i,3);
            ftemp(i,2)    =f(i,2);
        else
            ftemp(i+1,1)  =f(i,1);
            ftemp(i-1,3)  =f(i,3);
            ftemp(i,2)    =f(i,2);
        end
    end
end

%at x=0
i=1;
if ~sln(i) && ~sln_r(i)
    if (sln(i+1)~=0||sln_r(i+1)~=0) 
        ftemp(i,3)    =f(i,1);
        ftemp(i,2)    =f(i,2);
    else
        ftemp(i+1,1)  =f(i,1);
        ftemp(i,2)    =f(i,2);
    end
end

%at x=ly
i=ly;
if ~sln(i) && ~sln_r(i)
    if (sln(i-1)~=0||sln_r(i-1)~=0) 
        ftemp(i,1)    =f(i,3);
        ftemp(i,2)    =f(i,2);
    else
        ftemp(i-1,3)  =f(i,3);
        ftemp(i,2)    =f(i,2);
    end
end

