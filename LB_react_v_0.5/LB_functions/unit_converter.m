function [l0,m0,t0,c0,f0,v0,tau]=unit_converter(delx,diffusion_coeff,varargin)

%default values
delx_lb=1;
delt_lb=1;
tau=1;

%reading additional data
if ~isempty(varargin)
    for i=1:length(varargin)
        if strcmp(varargin{i},'delx_lb')
            delx_lb = varargin{i+1};
        end
        if strcmp(varargin(i),'delt_lb')
            delt_lb = varargin{i+1};
        end
         if strcmp(varargin(i),'tau')
            tau = varargin{i+1};
         end
    end
end

%computing other lb parameters
v=delx_lb/delt_lb;

vs=v^2/3;
diffusivity_lb=(vs/delt_lb)*(tau/delt_lb-0.5);

%computing scaling parameters
m0=1;
l0=delx/delx_lb;

%taking into account variable diffusion coefficients
if length(diffusion_coeff)>1
    diffusivity_physical=min(diffusion_coeff);
else
    diffusivity_physical=diffusion_coeff;
end
t0=l0^2*(diffusivity_lb/diffusivity_physical); 

%computing tau in case of domain with variable coeff diff tau at diff
%nodes!
D0=10^2/t0;                                        %diffusivity coefficient factor
tau=((diffusion_coeff/D0)*3/vs+0.5)*delt_lb; 

%computing other factors
c0 = m0*l0^-3;                                     %concentration factor    
f0 = m0*l0^-2*t0^-1;                               %flux factor
v0 = l0*t0^-1;                                     %velocity factor
end