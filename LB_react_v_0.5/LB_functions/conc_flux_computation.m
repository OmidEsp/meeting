function [conc,flux] = conc_flux_computation(f,varargin)
%function to compute macroscopic quantites of D1Q3 lattice
%by default assume pure diffusion
%additionally one input velocities in case of advective diffusion 

%setting up input:
ly=size(f);
ly=ly(1);
u=0;        %by default pure diffusion
delt_lb =1; %by default lb timestep =1
v=1;
sln(1:ly)=0;
sln_r(1:ly)=0;
tau=1;
if ~isempty(varargin)
    for i=1:length(varargin)
        if strcmp(varargin{i},'u')
            u=varargin{i+1};
        end
         if strcmp(varargin{i},'tau')
            tau=varargin{i+1};
        end
        if strcmp(varargin{i},'sln')
            sln=varargin{i+1};
        end
        if strcmp(varargin{i},'sln_r')
            sln_r=varargin{i+1};
        end        
        if strcmp(varargin{i},'delt_lb')
            delt_lb=varargin{i+1};
        end
        if strcmp(varargin{i},'v')
            v=varargin{i+1};
        end
      
    end
end
    
%intialization
conc(1:ly)=0;
flux(1:ly)=0;

%computation of fluxes
if length(tau)==1
   if length(u)>1 
       for i=1:ly      
           if ~sln(i) && ~sln_r(i)
           conc(i)=f(i,1)+ f(i,2)+f(i,3);
           Dr=(1-(delt_lb/(2*tau)));
           flux(i)=(1-Dr)*(u(i).*conc(i)) -Dr*v*(f(i,3)-f(i,1));
           end
       end
   else
       for i=1:ly      
           if ~sln(i) && ~sln_r(i)
               conc(i)=f(i,1)+ f(i,2)+f(i,3);
               Dr=(1-(delt_lb/(2*tau)));
               flux(i)=(1-Dr)*(u.*conc(i)) -Dr*v*(f(i,3)-f(i,1));
           end
       end
   end
end

if length(tau)>1
    if length(u)>1
        for i=1:ly      
           if ~sln(i) && ~sln_r(i)
               conc(i)=f(i,1)+ f(i,2)+f(i,3);
               Dr=(1-(delt_lb./(2.*tau(i))));
               flux(i)=(1-Dr)*(u(i).*conc(i)) -Dr*v.*(f(i,3)-f(i,1));
           end
        end
    else
        for i=1:ly      
           if ~sln(i) && ~sln_r(i)
               conc(i)=f(i,1)+ f(i,2)+f(i,3);
               Dr=(1-(delt_lb./(2.*tau(i))));
               flux(i)=(1-Dr)*(u.*conc(i))-Dr*v.*(f(i,3)-f(i,1));
           end
        end
    end
end
end
 




  