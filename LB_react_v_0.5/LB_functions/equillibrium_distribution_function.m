function feq = equillibrium_distribution_function(conc,varargin)

%Computes equillibrium distribution function for D1Q3 lattice
%by default assume pure diffusion
%additionally one input velocities in case of advective diffusion 

%setting up input
ly=length(conc);
u=0;   %by default pure diffusion
v=1;   %by default velocity between two lattice nodes =1
sln(1:ly)=0; %default no solids
sln_r(1:ly)=0; %default no solids reactive node

if ~isempty(varargin)
    for i=1:length(varargin)
        if strcmp(varargin{i},'u')
            u=varargin{i+1};
        end
        if strcmp(varargin{i},'v')
            v=varargin{i+1};
        end
        if strcmp(varargin{i},'sln')
            sln=varargin{i+1};
        end
        if strcmp(varargin{i},'sln_r')
            sln_r=varargin{i+1};
        end
    end
end

%intialization
feq=zeros(ly,3);
vs=v/3^0.5;

%computation of equillibrium distribution function

if length(u)>1 
    for i=1:ly;
        if ~sln(i) && ~sln_r(i)
            feq(i,1) = (1/6).*conc(i).* (1+ v.* u(i)./vs^2);
            feq(i,3) = (1/6).*conc(i).* (1- v.* u(i)./vs^2);
            feq(i,2) = (4/6).*conc(i);
        end
    end
else
    for i=1:ly;
        if ~sln(i) && ~sln_r(i)
            feq(i,1) = (1/6)*conc(i).* (1+ v* u/vs^2);
            feq(i,3) = (1/6)*conc(i).* (1- v* u/vs^2);
            feq(i,2) = (4/6)*conc(i);
        end
    end

end
end










