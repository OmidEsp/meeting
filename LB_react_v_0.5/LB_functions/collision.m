function fc = collision(f,feq,varargin) 
%computes collision step for lattice boltzmann
%switch reaction type ='no_rxn' computes collision step without any
%reaction 
%switch reaction type ='decay' computes collision step with first order
%decay
%switch reaction type ='source-sink' computes collision step with specified
%source sink.Kinetic reactions and higher order decay can be computed
%externally and introduced as a source sink terms.

%setting up input
ly=size(f);
ly=ly(1);
delt_lb=1;
rxn_type ='no_rxn';
sln(1:ly)=0;
sln_r(1:ly)=0;
tau=1;

if ~isempty(varargin)
    for i=1:length(varargin)
        if strcmp(varargin{i},'tau')
            tau=varargin{i+1};
        end
        if strcmp(varargin{i},'sln')
            sln=varargin{i+1};
        end
        if strcmp(varargin{i},'sln_r')
            sln_r=varargin{i+1};
        end
        if strcmp(varargin{i},'conc')
            conc=varargin{i+1};
        end
        if strcmp(varargin{i},'delt_lb')
            delt_lb = varargin{i+1};
        end
        if strcmp(varargin{i},'decay')
            rxn_type='decay';
            lemda_lb = varargin{i+1};
        end
        if strcmp(varargin{i},'source-sink')
            rxn_type='source-sink';
            S_lb    = varargin{i+1};
        end
        
    end
end


%intialization
fc = zeros(ly,3);
omega = delt_lb./tau;

switch rxn_type 
    case 'no_rxn'
        if length(omega)>1                                          %different diffusion coeff for diff nodes
            for i = 1:ly
                if ~sln(i) && ~sln_r(i)
                    fc(i,1)=(1-omega(i)).*f(i,1) +(omega(i).* feq(i,1));
                    fc(i,3)=(1-omega(i)).*f(i,3) +(omega(i).* feq(i,3)); 
                    fc(i,2)=(1-omega(i)).*f(i,2) +(omega(i).* feq(i,2));
                end                    
            end            
        else
           for i = 1:ly
               if ~sln(i) && ~sln_r(i)
                   fc(i,1)=(1-omega).*f(i,1) +(omega.* feq(i,1));
                   fc(i,3)=(1-omega).*f(i,3) +(omega.* feq(i,3)); 
                   fc(i,2)=(1-omega).*f(i,2) +(omega.* feq(i,2));
               end
           end
        end
    case 'decay'
        w(1)=1/6;w(2)=4/6;                        
        if length(omega)>1
            if length(lemda_lb)>1                                  %different decay rates at diff nodes
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega(i)).*f(i,1) + (omega(i).* feq(i,1))-(lemda_lb(i).*delt_lb.*conc(i)*w(1));
                        fc(i,3)=(1-omega(i)).*f(i,3) + (omega(i).* feq(i,3))-(lemda_lb(i).*delt_lb.*conc(i)*w(1));
                        fc(i,2)=(1-omega(i)).*f(i,2) + (omega(i).* feq(i,2))-(lemda_lb(i).*delt_lb.*conc(i)*w(2));
                    end
                end
                
            else
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega(i)).*f(i,1) + (omega(i).* feq(i,1))-(lemda_lb.*delt_lb.*conc(i)*w(1));
                        fc(i,3)=(1-omega(i)).*f(i,3) + (omega(i).* feq(i,3))-(lemda_lb.*delt_lb.*conc(i)*w(1));
                        fc(i,2)=(1-omega(i)).*f(i,2) + (omega(i).* feq(i,2))-(lemda_lb.*delt_lb.*conc(i)*w(2));
                    end
                end
            end
            
        else
            if length(lemda_lb)>1                                  %different decay rates at diff nodes
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega)*f(i,1) + (omega * feq(i,1))-(lemda_lb(i).*delt_lb.*conc(i)*w(1));
                        fc(i,3)=(1-omega)*f(i,3) + (omega * feq(i,3))-(lemda_lb(i).*delt_lb.*conc(i)*w(1));
                        fc(i,2)=(1-omega)*f(i,2) + (omega * feq(i,2))-(lemda_lb(i).*delt_lb.*conc(i)*w(2));
                    end
                end
            else
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega)*f(i,1) + (omega * feq(i,1))-(lemda_lb.*delt_lb.*conc(i)*w(1));
                        fc(i,3)=(1-omega)*f(i,3) + (omega * feq(i,3))-(lemda_lb.*delt_lb.*conc(i)*w(1));
                        fc(i,2)=(1-omega)*f(i,2) + (omega * feq(i,2))-(lemda_lb.*delt_lb.*conc(i)*w(2));
                    end
                end
            end
        end
    case 'source-sink'
        w(1)=1/6;w(2)=4/6;
        if length(omega)>1
            if length(S_lb)>1                                  %different source/sink terms at diff nodes
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega(i)).*f(i,1) + (omega(i).* feq(i,1))+(S_lb(i)*w(1));
                        fc(i,3)=(1-omega(i)).*f(i,3) + (omega(i).* feq(i,3))+(S_lb(i)*w(1));
                        fc(i,2)=(1-omega(i)).*f(i,2) + (omega(i).* feq(i,2))+(S_lb(i)*w(2));
                    end
                end
            else
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega(i)).*f(i,1) + (omega(i).* feq(i,1))+(S_lb*w(1));
                        fc(i,3)=(1-omega(i)).*f(i,3) + (omega(i).* feq(i,3))+(S_lb*w(1));
                        fc(i,2)=(1-omega(i)).*f(i,2) + (omega(i).* feq(i,2))+(S_lb*w(2));
                    end
                end
            end
        else
            if length(S_lb)>1                                  %different decay rates at diff nodes
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega)*f(i,1) + (omega * feq(i,1))+(S_lb(i)*w(1));
                        fc(i,3)=(1-omega)*f(i,3) + (omega * feq(i,3))+(S_lb(i)*w(1));
                        fc(i,2)=(1-omega)*f(i,2) + (omega * feq(i,2))+(S_lb(i)*w(2));
                        
                    end
                end
            else
                for i=1:ly
                    if ~sln(i) && ~sln_r(i)
                        fc(i,1)=(1-omega)*f(i,1) + (omega * feq(i,1))+(S_lb*w(1));
                        fc(i,3)=(1-omega)*f(i,3) + (omega * feq(i,3))+(S_lb*w(1));
                        fc(i,2)=(1-omega)*f(i,2) + (omega * feq(i,2))+(S_lb*w(2));
                    end
                end
            end
        end
end
end
