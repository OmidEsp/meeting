function ftemp =  boundary_condition(f,varargin)
%imposes different boundary conditions
%boundary conditions can be periodic,no flux,constant concentration and 
%by default periodic boundary are imposed

%intialization
ly=size(f);
ly=ly(1);
ftemp =f;

%default no bc
bc_flag_top   ='no_bc';
bc_flag_bottom='no_bc';

%flags for conc or flux boundary at top and bottom
if ~isempty(varargin)
    for i=1:length(varargin)
        if strcmp(varargin{i},'top_conc_bc')
            bc_flag_top = 'conc_bc';
        end
        
        if strcmp(varargin{i},'top_flux_bc')
            bc_flag_top = 'flux_bc';
        end
        
        if strcmp(varargin{i},'top_periodic_bc')
            bc_flag_top = 'periodic_bc';
        end
        
        if strcmp(varargin{i},'bottom_conc_bc')
            bc_flag_bottom = 'conc_bc';
        end
        
        if strcmp(varargin{i},'bottom_flux_bc')
            bc_flag_bottom = 'flux_bc';
        end
        
        if strcmp(varargin{i},'bottom_periodic_bc')
            bc_flag_bottom = 'periodic_bc';
        end
    end
end

%imposing boundary conditions at x=0
switch bc_flag_top
    case 'conc_bc'
        %default input
        type = 'nodal'; %default flag
        %reading input
        if ~isempty(varargin)
           for i=1:length(varargin)
               if strcmp(varargin{i},'type')
                   type = varargin{i+1};
               end
               if strcmp('c_0',varargin{i})
                   c_0 = varargin{i+1};
               end
               
           end
       end
       switch type
           case 'midway'
               c_1 = f(2,3)+f(2,2)+f(2,1);
               ftemp(1,1)=(2 * c_0 -c_1)-(f(1,3)+f(1,2));
           case 'nodal'
               ftemp(1,1)= c_0 - (f(1,3)+f(1,2));
       end
       
    case 'flux_bc'
        u=0;tau=1;delt_lb=1;v=1; type = 'nodal';         %default values
        if ~isempty(varargin)
           for i=1:length(varargin)
               if strcmp(varargin{i},'type')
                   type = varargin{i+1};
               end
               if strcmp(varargin{i},'flux_0')
                   flux_0 = varargin{i+1};
               end
               if strcmp(varargin{i},'u')
                   u = varargin{i+1};
                   u=u(1);
               end
               if strcmp(varargin{i},'delt_lb')
                   delt_lb = varargin{i+1};
               end
               if strcmp(varargin{i},'tau')
                   tau = varargin{i+1};
                   tau = tau(1);
               end
               if strcmp(varargin{i},'v')
                   v = varargin{i+1};
               end
           end
        end
       switch type
           case 'nodal'
               Dr = 1 -(delt_lb/(2*tau));
               ftemp(1,1)=(flux_0-((1-Dr)*u)*f(1,2)+((v+u)*Dr-u)*f(1,3))/(u+(v-u)*Dr);
              
           case 'midway'
               flux_1 = (f(2,1)-f(2,3))*v;
               flux_b = 2*flux_0 - flux_1 ;
               Dr = 1 -(delt_lb/(2*tau));
               ftemp(1,1)=(flux_b-((1-Dr)*u)*f(1,2)+((v+u)*Dr-u)*f(1,3))/(u+(v-u)*Dr);
       end
       
    case 'periodic_bc'
        ftemp(1,1)=f(ly,1);  
    
    case 'no_bc'
        ftemp(1,1)=f(1,1);
        
end
%imposing boundary conditions at x=ly
switch bc_flag_bottom
    case 'conc_bc'
        %default input
        type = 'nodal'; %default flag
        %reading input
        if ~isempty(varargin)
           for i=1:length(varargin)
               if strcmp(varargin{i},'type')
                   type = varargin{i+1};
               end
               if strcmp(varargin{i},'c_ly')
                   c_ly = varargin{i+1};
               end
              
           end
       end
       switch type
           case 'midway'
               c_ly_1 = f(ly-1,3)+f(ly-1,2)+f(ly-1,1);
               ftemp(ly,3)=2*c_ly -c_ly_1 -f(ly,1)-f(ly,2);
               
           case 'nodal'
               ftemp(ly,3)=c_ly-f(ly,1)-f(ly,2);
       end
       
    case 'flux_bc'
        u=0;tau=1;delt_lb=1;v=1; type = 'nodal';         %default values
        if ~isempty(varargin)
           for i=1:length(varargin)
               if strcmp(varargin{i},'type')
                   type = varargin{i+1};
               end
               if strcmp(varargin{i},'flux_ly')
                   flux_ly = varargin{i+1};
               end
               
               if strcmp(varargin{i},'u')
                   u = varargin{i+1};
                   u =u(1);
               end
               if strcmp(varargin{i},'delt_lb')
                   delt_lb = varargin{i+1};
               end
               if strcmp(varargin{i},'tau')
                   tau = varargin{i+1};
                   tau = tau(1);
               end
           end
        end
       switch type
           case 'nodal'
               Dr = 1 -(delt_lb/(2*tau));
               ftemp(ly,3)=-(flux_ly-((1-Dr)*u)*f(ly,2)-((u+(v-u)*Dr)*f(ly,1))/((v+u)*Dr-u));
               
           case 'midway'
               flux_1 = (f(ly-1,1)-f(ly-1,3))*v;
               flux_b = 2*flux_ly - flux_1 ;
               Dr = 1 -(delt_lb/(2*tau));
               ftemp(ly,3)=-(flux_b-((1-Dr)*u)*f(ly,2)-((u+(v-u)*Dr)*f(ly,1))/((v+u)*Dr-u));
       end
    case 'periodic_bc'
        ftemp(ly,3)=f(1,3);   
    
    case 'no_bc'
        ftemp(ly,3)=f(ly,3);
end

end
    


