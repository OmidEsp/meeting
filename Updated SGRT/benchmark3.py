from __future__ import division,print_function
__doc__= """
Benchmark: single component reactive transport model with moving boundary
"""
#%% import modules
import sys,os
PARENT1 = '..'
YANTRA_PATH = '../../../yantra-dev'
sys.path.append(os.path.join(os.path.dirname(__file__), PARENT1))
os.environ['OMP_NUM_THREADS']=str(4)
sys.path.append(YANTRA_PATH)
import yantra
from scipy.optimize import root
from SingleCompReactTrans import SingleCompReactTrans
import numpy as np
#import random
import matplotlib.pylab as plt
#%% parameters for the simulations
dx= 1
lx = 100
ly = 80
D = 1/6
Da= 4
kr  = D*Da/ly
ceq= 50
cb= 100.
cp= 0.001
mv=1
#%%analytical solution
#from original solution beta*ly is taken as beta
    
def any_c():
    global ly,lx,x,y,Da
    sin,cosh,cos,tan = np.sin,np.cosh,np.cos,np.tan
    nbetas=65
    betalist = []    
    trans_fun=lambda(t):t*tan(t)-Da  
    count=2
    i=1
    betalist.append(root(trans_fun,i,options={'xtol':1e-13,'ftol':1e-13,'maxiter':5000,'factor':0.1}, method='lm').x)
    while count<=nbetas:
        i=i+1
        beta=root(trans_fun,i,options={'xtol':1e-13,'ftol':1e-13,'maxiter':5000,'factor':0.1}, method='lm').x
        if abs(betalist[-1]-beta)>1e-5:
            if abs(beta*tan(beta)-Da)<1e-5:
                betalist.append(beta)
                count=count+1
    betalist = [beta/ly for beta in betalist]
    cn = 0
    for beta in betalist:
        Nnsq=ly/2.*(1+(sin(2.*beta*ly)/(2*beta*ly)))
        term1=(sin(beta*ly)/(Nnsq*beta))
        term2=cosh(beta*(x-lx))/cosh(beta*lx)
        term3=cos(beta*y)
        cn+=term1*term2*term3
    return cn
#%%create yantra domain instance
domain = yantra.Domain2D((0,0),(lx,ly+dx),dx, grid_type = 'midway')
domain.nodetype[0,:]=1
x,y=domain.meshgrid()
cs = cp* (domain.nodetype > 0)
#%%create physics instance
#domain params
domain_params={}
domain_params['D'] = D
domain_params['rxntype']='kin'
domain_params['ceq']=ceq
domain_params['c']=ceq
domain_params['kr']=kr
domain_params['mv']=mv
domain_params['thres']=0.5
domain_params['cs']=cs
#set boundary conditions
#concentration 0  on left boundary and zero flux on right hand boundary
bc_params ={}
bc_params['left']=['c' ,cb]
bc_params['right']=['flux',0]
bc_params['top']=['flux' ,0]
bc_params['bottom']=['flux',0]

#solver parameters
solver_params={}
solver_params['lattice']='D2Q5'
solver_params['collision_model']='trt'
solver_params['tauref'] = 1
#create physics instance
rt = SingleCompReactTrans(domain,domain_params,bc_params,solver_params)
#%%run model (geometry update is supressed for this benchmark)
for i in range(200000):
        rt.time +=rt.dt
        rt.iters+=1
        rt.collide()
        rt.stream()
        rt.apply_bc()
        rt.hetrxn()
        rt.compute_macro_var()
#%%plot results
ac=any_c()
cn = (rt.c - ceq)/(cb-ceq)
cs=plt.contour(x[1:,:],y[1:,:],ac[1:,:],levels=np.linspace(0,1,11),colors='b', )
cs=plt.contour(x[1:,:],y[1:,:],cn[1:,:],levels=np.linspace(0,1,11),colors='k',linestyles='--')
plt.clabel(cs, inline=1, fontsize=20,fmt='%1.1f')
plt.axis('image')
plt.show()
