from __future__ import division,print_function
__doc__= """ 
Benchmark: single component reactive transport model with moving boundary  (dissolution)
"""
#%% import modules
import sys,os
PARENT1 = '..'
YANTRA_PATH = '../../../yantra-dev'
sys.path.append(os.path.join(os.path.dirname(__file__), PARENT1))
sys.path.append(YANTRA_PATH)
import yantra
from SingleCompReactTrans import SingleCompReactTrans
from scipy.special import erfc
from scipy.optimize import fsolve
import numpy as np
import matplotlib.pylab as plt
#%%Analytical solution
#reference:
#Aaron, et al. (1970). Diffusion-Limited Phase Transformations: 
#A Comparison and Critical Evaluation of the Mathematical Approximations.
#Journal of applied physics, 41(11), 4404-4410.
def transfunc(lmda):
    global ci,cm,cp
    k =  2*(ci-cm)/(cp-ci)
    res = np.pi**0.5 * lmda * np.exp(lmda**2) * erfc(-lmda) - k/2
    return res
def cany():
    global ci,x,cm,cp,x0,D,tf
    lmda = fsolve(transfunc,0.1)[0]
    c = cm + (ci-cm) * erfc(0.5*(x0-x)/(D*tf)**0.5)/(erfc(-lmda))
    xi =x0 + dxiany() 
    c = c *(x<=xi) + cp *(x>xi)
    return c
def dxiany():
    global D,t
    lmda = fsolve(transfunc,0.1)[0]
    lmda1 = lmda*2
    dxi= lmda1 *(D*t)**0.5
    return dxi
#%% parameters for the simulations
ci= 0.5
cm= 0
cp= 1
dx= 1.0
x0= 100
D = 1./6.
#%%create yantra domain instance
domain = yantra.Domain2D((0,0),(200,10),dx, grid_type = 'midway')
x = domain.x
xx, _ = domain.meshgrid()
domain.nodetype =1. * (xx >= x0)
cs = cp* (domain.nodetype > 0)
#%%create physics instance
#domain params 
domain_params={}
domain_params['D'] = D
domain_params['rxntype']='eq'
domain_params['ceq']=ci
domain_params['c']=cm
domain_params['mv']=1/cp
domain_params['thres']=0.5
domain_params['cs']=cs
#set boundary conditions
#concentration 0  on left boundary and zero flux on right hand boundary
bc_params ={}
bc_params['left']=['c' ,0.0]
bc_params['right']=['flux',0.0]
#solver parameters
solver_params={}
solver_params['lattice']='D2Q5'
solver_params['collision_model']='srt'

#create physics instance
rt = SingleCompReactTrans(domain,domain_params,bc_params,solver_params)

#%%run model
dxia=[]
dxim=[]
tl = []
xlb0=x[np.argmax(rt.nodetype[1,:])]
while rt.time <=5000:
    rt.advance()
    t = rt.time
    tl.append(t)
    dxim.append(x[np.argmax(rt.nodetype[1,:])]-xlb0-dx/2) #-dx/2 added gives center of location of fluid interface node
    dxia.append(dxiany())

#%%plot output    
tf =t= rt.time
c = cany() 
cm = rt.c[1,:]+rt.cs[1,:]
plt.figure()
plt.plot(x,c,label = 'analytical')
plt.plot(x,cm,'--',label = 'model')
plt.legend()
plt.show()
plt.figure()
plt.plot(tl,dxia,label = 'analytical')
plt.plot(tl,dxim,'--',label = 'model')
plt.legend()
plt.show()
