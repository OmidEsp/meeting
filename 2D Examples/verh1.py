from __future__ import division,print_function
__doc__= """ 
Benchmark: single component reactive transport model with moving boundary
"""
#%% import modules
import sys,os
PARENT1 = '../'
YANTRA_PATH = '/home/ravi/switchdrive/codes/yantra_dev/yantra-dev'
sys.path.append(os.path.join(os.path.dirname(__file__), PARENT1))
sys.path.append(YANTRA_PATH)
import yantra
from SingleCompReactTrans import SingleCompReactTrans
from scipy.special import erfc
from scipy.optimize import fsolve
import numpy as np
import matplotlib.pylab as plt
#%% parameters for the simulations
ceq= 0.6
#kr = 0.01
cb= 0.5
cp= 0.9
dx = 1
VF = 0.01
D = 1./6.
niters =5000
#%%create yantra domain instance
domain = yantra.Domain2D((0,0),(150,100),dx, grid_type = 'midway')
domain.draw_circle((50,50),13,idx=1)
#domain.draw_circle((l,0),r)
#domain.draw_circle((0,l),r)
#domain.draw_circle((0,0),r)
#domain.draw_circle((l,l),r)
cs = cp* (domain.nodetype > 0)
#%%create physics instance
#domain params 
domain_params={}
domain_params['D'] = D
domain_params['rxntype']='eq'
domain_params['ceq']=ceq
domain_params['c']=cb
#domain_params['kr']=kr
domain_params['mv']=1
domain_params['thres']=0.5
domain_params['cs']=cs
#set boundary conditions
#concentration 0  on left boundary and zero flux on right hand boundary
bc_params ={}
bc_params['left']=['flux' ,0]
bc_params['right']=['flux',0]
bc_params['top']=['flux' ,0]
bc_params['bottom']=['flux',0]

#solver parameters
solver_params={}
solver_params['lattice']='D2Q5'
solver_params['collision_model']='srt'
solver_params['tauref']=1
#create physics instance
rt = SingleCompReactTrans(domain,domain_params,bc_params,solver_params)

#%%run model
dxia=[]
dxim=[]
tl = []
for i in range(niters):
    rt.advance()
#    plt.figure()
    cm=rt.c[:,:]+rt.cs[:,:]

    if i%100==0:
        hh=plt.contour(cm)
        plt.clabel(hh, inline=1, fontsize=10,fmt='%2.1f')
        plt.axis('image')
        plt.colorbar()
        plt.clim(0.0,0.9)
        plt.savefig('rsl/ct%i.png'%i)
        plt.clf()
        plt.imshow(cm)
        plt.colorbar()
        plt.clim(0.5,0.9)
        plt.savefig('rsl/conc%i.png'%i)
        plt.clf()
        plt.imshow(rt.cs)
        plt.savefig('rsl/sol%i.png'%i)
        plt.clf()
#    plt.show()
#    plt.figure()
#    plt.imshow(rt.c
